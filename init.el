(when load-file-name
  (setq user-emacs-directory (file-name-directory load-file-name)))

;;el-get
(add-to-list 'load-path (locate-user-emacs-file "el-get/el-get"))
(unless (require 'el-get nil 'noerror)
  (with-current-buffer
      (url-retrieve-synchronously
       "https://raw.githubusercontent.com/dimitri/el-get/master/el-get-install.el")
    (goto-char (point-max))
    (eval-print-last-sexp)))

;;インストールするパッケージ
(el-get-bundle auto-complete)
(el-get-bundle! emmet-mode)
(el-get-bundle! web-mode)
(el-get-bundle haskell-mode)
(el-get-bundle! open-junk-file)
(el-get-bundle! yasnippet)
(el-get-bundle! popwin)
(el-get-bundle tuareg-mode)
(el-get-bundle! rhtml-mode)
(el-get-bundle! smartparens)
(el-get-bundle scss-mode)
(el-get-bundle markdown-mode)
(el-get-bundle! undo-tree)
(el-get-bundle! undohist)
(el-get-bundle! git-gutter-fringe) ;;plusにしたほうがよい？
(el-get-bundle! hlinum)
;(el-get-bundle! point-undo) 使ってないから保留
(el-get-bundle anzu)
(el-get-bundle exec-path-from-shell) ;;flycheckのインストールコケるから入れた
(exec-path-from-shell-initialize)
(el-get-bundle flycheck)
(el-get-bundle! expand-region)
(el-get-bundle! migemo)
(el-get-bundle! helm)
(el-get-bundle! helm-descbinds)
(el-get-bundle! helm-ls-git)
(el-get-bundle! helm-swoop)
(el-get-bundle! helm-git-grep)
(el-get-bundle! helm-ag)
(el-get-bundle auctex)
(el-get-bundle! yaml-mode)
(el-get-bundle coffee-mode)
(el-get-bundle! magit)

(setq backup-directory-alist
      `((".*" . ,"~/.emacs.d/backups")))
(setq auto-save-file-name-transforms
      `((".*" ,"~/.emacs.d/backups" t)))

;; 文字コードを指定する
(set-language-environment "Japanese")
(prefer-coding-system 'utf-8)

;; Mac OS Xの場合のファイル名の設定
(when (eq system-type 'darwin)
  (require 'ucs-normalize)
  (set-file-name-coding-system 'utf-8-hfs)
  (setq locale-coding-system 'utf-8-hfs))

;;color-theme
(add-to-list 'custom-theme-load-path
	     (file-name-as-directory "~/.emacs.d/themes/"))
(load-theme 'dark-laptop t)
;(set-frame-parameter nil 'alpha 90)
(add-to-list 'default-frame-alist '(alpha . 90))

;;;  C-hとC-?を入れ替える
(keyboard-translate ?\C-h ?\C-?)

;; "C-t" でウィンドウを切り替える
(define-key global-map (kbd "C-.") 'other-window)

;;バーのとこ消す
(tool-bar-mode 0)
(menu-bar-mode 0)

;;scratchバッファで評価した結果を省略しないようにする
(setq eval-expression-print-length nil)
(setq eval-expression-print-level nil)

;; モードラインにカラム番号も表示
(column-number-mode t)

;;; P90 タイトルバーにファイルのフルパスを表示
(setq frame-title-format "%f")

;; 行番号を常に表示する
(global-linum-mode t)
;; 対応するカッコを明るく
(show-paren-mode t)
;; 重いらしい処理を軽く
(setq linum-delay t)
(defadvice linum-schedule (around my-linum-schedule () activate)
  (run-with-idle-timer 0.2 nil #'linum-update-current))

;;フォントをrictyに
;;mac
(when (eq system-type 'darwin)
  (add-to-list 'default-frame-alist `(font . "ricty-14"))
  ;; (set-face-attribute 'default nil
  ;;                     :family "Ricty"
  ;;                     :height 130)
  ;; (set-fontset-font nil 'japanese-jisx0208
  ;;                   (font-spec :family "Ricty"))
  ;;起動時のサイズ指定
  (setq default-frame-alist
        (append '((width . 110)
                  (height . 47))
                default-frame-alist))
  (setq initial-frame-alist default-frame-alist)  
  )
;;linux
(when (eq system-type 'gnu/linux)
  (set-face-attribute 'default nil
                      :family "Ricty"
                      :height 105)
  (set-fontset-font nil 'japanese-jisx0208
                    (font-spec :family "Ricty"))
  ;;起動時のサイズ指定
  (setq default-frame-alist
        (append '((width . 110)
                  (height . 50))
                default-frame-alist))
  (setq initial-frame-alist default-frame-alist)
  )

;; TRAMPでバックアップファイルを作成しない
(add-to-list 'backup-directory-alist
             (cons tramp-file-name-regexp nil))

;;ruby-mode
(setq ruby-insert-encoding-magic-comment nil)

;; ruby-modeの拡張子対応
(add-to-list 'auto-mode-alist '("Gemfile" . ruby-mode))
(add-to-list 'auto-mode-alist '("\\.thor$" . ruby-mode))
(add-to-list 'auto-mode-alist '("\\.rake$" . ruby-mode))
(add-to-list 'auto-mode-alist '("Guardfile$" . ruby-mode))

;; 補完を再設定 M-/をre-bind これも保留
;(global-set-key (kbd "C-j") 'dabbrev-expand)

;;M-hで前方単語削除
(global-set-key (kbd "M-h") 'backward-kill-word)

;; パラグラフ移動（空行移動） auto-complete関わりで保留
;(global-set-key (kbd "M-n") 'forward-paragraph)
					;(global-set-key (kbd "M-p") 'backward-paragraph)

;;scroll
(setq scroll-conservatively 35
      scroll-margin 0
      scroll-step 1)

;;cursor
(add-to-list 'default-frame-alist '(cursor-color . "light gray"))

;;;uniqufy 同一バッファ名を見やすく
(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward-angle-brackets)

;; 変更のあったファイルの自動再読み込み
(global-auto-revert-mode 1)

;;;以下要パッケージインストール
;;auto-completeの設定
(when (require 'auto-complete-config nil t)
  (add-to-list 'ac-dictionary-directories "~/.emacs.d/ac-dict")
  (setq ac-use-comphist t)
  (define-key ac-mode-map (kbd "M-TAB") 'auto-complete)
  ;;menu表示時のキーバインド
  (setq ac-use-menu-map t)
  (define-key ac-menu-map "\C-n" 'ac-next)
  (define-key ac-menu-map "\C-p" 'ac-previous)
  (ac-config-default))


;;emmet-mode
;(require 'emmet-mode)
(add-hook 'sgml-mode-hook 'emmet-mode) ;; マークアップ言語全部で使う
(add-hook 'css-mode-hook  'emmet-mode) ;; CSSにも使う
(add-hook 'web-mode-hook  'emmet-mode) ;; web-modeで使う
(add-hook 'emmet-mode-hook (lambda () (setq emmet-indentation 2))) ;; indent はスペース2個
;; (eval-after-load "emmet-mode"
;;   '(define-key emmet-mode-keymap (kbd "C-j") nil)) ;; C-j は newline のままにしておく

;;以下二文なぜかno-windowモードでバグったので保留
;;(keyboard-translate ?\C-i ?\H-i) ;;C-i と Tabの被りを回避
;;(define-key emmet-mode-keymap (kbd "H-i") 'emmet-expand-line) ;; C-i で展開


;;web-mode
;(require 'web-mode)

;;; 適用する拡張子
(add-to-list 'auto-mode-alist '("\\.phtml$"     . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php$" . web-mode))
(add-to-list 'auto-mode-alist '("\\.jsp$"       . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x$"   . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb$"       . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?$"     . web-mode))

(add-to-list 'auto-mode-alist '("\\.ctp?$"     . web-mode))
(add-to-list 'auto-mode-alist '("\\.php?$"     . web-mode))
(setq web-mode-engines-alist
      '(("php"    . "\\.phtml\\'"))
)

;;; インデント数 ;railsのときは全部2にしてた
(defun web-mode-hook ()
  "Hooks for Web mode."
  (setq web-mode-markup-indent-offset   4)
  (setq web-mode-css-indent-offset    4)
  ;(setq web-mode-script-offset 2)
  (setq web-mode-code-indent-offset 4)
  (setq web-mode-indent-style 4)
  ;(setq web-mode-asp-offset    2)
  (setq indent-tabs-mode nil)
  )
(add-hook 'web-mode-hook 'web-mode-hook)


(add-to-list 'ac-modes 'web-mode)
(add-to-list 'ac-modes 'tuareg-interactive-mode)

;;;;gtags 保留
;; (autoload 'gtags-mode "gtags" "" t)
;; (setq gtags-mode-hook
;;       '(lambda ()
;;          (local-set-key "\M-t" 'gtags-find-tag)
;;          (local-set-key "\M-r" 'gtags-find-rtag)
;;          (local-set-key "\M-s" 'gtags-find-symbol)
;;          (local-set-key "\M-p" 'gtags-pop-stack)
;;          (setq gtags-select-buffer-single t)
;;          ))

;;
;; (add-hook 'c-mode-common-hook
;;           '(lambda()
;;              (gtags-mode 1)
;;              (gtags-make-complete-list)
;;              ))

;;haskell-mode indent
;(custom-set-variables
; '(haskell-mode-hook '(turn-on-haskell-indentation)))
(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)


;;junk
;(require 'open-junk-file)
(setq open-junk-file-format "~/junk/%Y/%m-%d-%H%M%S.")
(setq open-junk-file-find-file-function 'find-file) ;;カレントウインドウで開く

;;yasnippet
;(require 'yasnippet)
(setq yas-snippet-dirs
      '("~/.emacs.d/snippets"                 ;; personal snippets
        "~/.emacs.d/el-get/yasnippet/snippets"
        ))

(yas-global-mode 1) ;; or M-x yas-reload-all if you've started YASnippet already.

;;popwin
;(require 'popwin)
(popwin-mode 1)
(setq display-buffer-function 'popwin:display-buffer)
(global-set-key (kbd "C-z") popwin:keymap) ;保留

;;ocaml tuareg-mode
(add-hook
 'tuareg-mode-hook
 '(lambda ()
    ;; indentation rules
    (setq tuareg-lazy-= t)
    (setq tuareg-lazy-paren t)
    (setq tuareg-in-indent 0)
    (setq tuareg-electric-indent nil)
    (setq tuareg-leading-star-in-doc t)
    (setq tuareg-with-indent 0)

    (setq tuareg-library-path "/usr/local/lib/ocaml/")

    ;; turn on auto-fill minor mode
    (auto-fill-mode 1)

    ;; Sym-Lock customization only
    ;; turn off special face under mouse
    (if (featurep 'sym-lock)
        (setq sym-lock-mouse-face-enabled nil))

    ;; ocamlspot and other keys
    (local-set-key "\C-c;" 'ocamlspot-query)
    (local-set-key "\C-c\C-t" 'ocamlspot-type)
    (local-set-key "\C-c\C-y" 'ocamlspot-type-and-copy)
    (local-set-key "\C-c\C-u" 'ocamlspot-use)
    (local-set-key "\C-cT" 'caml-types-show-type)
    ))

;;; rhtml-mode
;(require 'rhtml-mode)
(add-hook 'rhtml-mode-hook
    (lambda () (rinari-launch)))

;;smartparens
;(require 'smartparens)
;;rubyでendに色
(eval-after-load "ruby-mode"
  '(progn
     (require 'smartparens-ruby)
     (set-face-attribute 'sp-show-pair-match-face nil
                         :background "grey20" :foreground "green"
                         :weight 'semi-bold)))
;;デフォルトの「いいかんじ」の設定
;; (require 'smartparens-config)
;; (smartparens-global-mode t)

(add-hook 'ruby-mode-hook 'show-smartparens-mode)

;;scss-mode
(setq scss-compile-at-save nil)

;;markdown
(autoload 'markdown-mode "markdown-mode"
   "Major mode for editing Markdown files" t)
(add-to-list 'auto-mode-alist '("\\.text\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))
;;markdownからmarkdown-pyへ
(setq markdown-command "markdown_py")
;(add-hook 'markdown-mode-hook (lambda () (yas/dont-activate)))
;(add-hook 'markdown-mode-hook (lambda () (make-variable-buffer-local 'yas/dont-active)))
(add-hook 'markdown-mode-hook (lambda ()
                                (setq yas/minor-mode 'nil)
                                (setq tab-width 4)          ;;タブ幅4
                                (setq indent-tabs-mode t)))

;;undo-tree
;;(require 'undo-tree)
(global-undo-tree-mode t)
(global-set-key (kbd "C-?") 'undo-tree-redo)

;;undohist
;(require 'undohist)
(undohist-initialize)

;;git-gutter
;(require 'git-gutter-fringe)
(global-git-gutter-mode +1)

;;hlinum
;(require 'hlinum)
(hlinum-activate)

;point-undo
;(require 'point-undo)
(define-key global-map [f7] 'point-undo)
(define-key global-map [S-f7] 'point-redo)

;;anzu
(global-anzu-mode +1)

;flycheck
;(add-hook 'ruby-mode-hook 'flycheck-mode)
(add-hook 'ruby-mode-hook
          '(lambda ()
             (setq flycheck-checker 'ruby-rubocop)
             (flycheck-mode 1)))

;;expand-region
;(require 'expand-region)
(global-set-key (kbd "C-M-SPC") 'er/expand-region) ;C-M-SPC
(global-set-key (kbd "<C-M-return>") 'er/contract-region) ;C-M-return
;; transient-mark-modeが nilでは動作しませんので注意
(transient-mark-mode t)

;;migemo 
(setq migemo-command "cmigemo")
(setq migemo-options '("-q" "--emacs"))

(setq migemo-dictionary
      (expand-file-name "/usr/local/share/migemo/utf-8/migemo-dict"))

(setq migemo-user-dictionary nil)
(setq migemo-regex-dictionary nil)
(setq migemo-coding-system 'utf-8-unix)
(migemo-init)

;;helm
(require 'helm-config)
(helm-mode 1)
(helm-migemo-mode +1)
(define-key global-map (kbd "M-y") 'helm-show-kill-ring)
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "C-x b") 'helm-buffers-list)
(global-set-key (kbd "C-M-o") 'helm-occur)

;;helm-descbinds
(helm-descbinds-mode)

;;helm-git-grep
(global-set-key (kbd "C-c g") 'helm-git-grep)
(global-set-key (kbd "C-c p") 'helm-git-grep-at-point)

;;helm-ls-git
(global-set-key (kbd "C-c l") 'helm-ls-git-ls)

;;helm-ag
(global-set-key (kbd "C-c a") 'helm-ag)

;;ganma作
;(require 'emacs-surround)
;(global-set-key (kbd "C-q") 'emacs-surround)

;;coffee
(custom-set-variables '(coffee-tab-width 2))

;;magit
(global-set-key (kbd "C-x g") 'magit-status)
